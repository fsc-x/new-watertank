
#define MyAppName "NT1300"
#define MyAppVersion "2.0.0"
#define MyAppPublisher "�Ĵ����ؿƼ����޹�˾"
#define MyAppURL "http://www.example.com/"
#define MyAppExeName "NT1300.exe"
[Setup]
; �? AppId的值为单独标识该应用程序�?
; 不要为其他安装程序使用相同的AppId值�?
; (生成新的GUID，点�?工具|在IDE中生成GUID�?
AppId={{B5C32D6C-B958-4691-8C40-7A578B3B0C36}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={pf}\{#MyAppName}
DefaultGroupName={#MyAppName}
OutputDir=D:\sczc\new-watertank\dist
OutputBaseFilename=NT1300_windows_x64_setup_v2.0.0
SetupIconFile=D:\sczc\new-watertank\app.ico
Compression=lzma
SolidCompression=yes

[Languages]
; Name: "chinesesimp"; MessagesFile: "compiler:Default.isl"
Name: "english"; MessagesFile: "compiler:Default.isl"
[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: checkablealone; OnlyBelowVersion: 0,6.1
Name: "quicklaunchicon"; Description: "{cm:CreateQuickLaunchIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: checkablealone
[Files]
Source: "D:\sczc\new-watertank\dist\NT1300\NT1300.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\sczc\new-watertank\dist\NT1300\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
; Source: "F:\Python\Git\PersonalDose\dist\main\DoseManager.exe"; DestDir: "{app}"; Flags: ignoreversion
; Source: "F:\Python\Git\PersonalDose\dist\main\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
; 注意: 不要在任何共享系统文件上使用“Flags: ignoreversion�?
[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{group}\{cm:ProgramOnTheWeb,{#MyAppName}}"; Filename: "{#MyAppURL}"
Name: "{group}\{cm:UninstallProgram,{#MyAppName}}"; Filename: "{uninstallexe}"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: quicklaunchicon
Name: "{userdesktop}\{#MyAppName}";Filename: "{app}\{#MyAppExeName}"; WorkingDir: "{app}"
[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent
