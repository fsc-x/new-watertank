#!/usr/bin/env python
# encoding: utf-8

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import os
import configparser
import binascii
from common.libs.ComDeal import WATER_CMD

from ui.envMeasure import Ui_Dialog
class EnvironmentMeasureDialog(QDialog,Ui_Dialog):
    signal_serial_send = pyqtSignal(list)
    measure_flag = False
    def __init__(self,parent=None, serial=None):
        super(QDialog, self).__init__(parent)
        self.setupUi(self)
        self.setWindowIcon(QIcon(':icons/images_rc/box.png'))
        self.setWindowTitle("本底测量")
        # 配置文件读取
        # self.readConfig()
        self.period = 200
        # 串口模块
        self.serial = serial
        self.pushButton_start.clicked.connect(self.start)
        self.progressBar.setRange(0, 100)
        self.progressBar.setValue(0)

    def readConfig(self):
        self.configfile = os.getcwd()+'/conf/base.ini'
        config = configparser.ConfigParser()
        config.read(self.configfile)
        self.period = int(config['base']['period'])

    def start(self):
        self.signal_serial_send.emit(WATER_CMD['本底测量'])
        self.measure_flag = True

    def serial_deal(self, cmd, data):
        if self.measure_flag:
            if cmd == '72':  # 本底测量
                t = int(data, 16) * self.period / 1000
                t1 = float('%0.1f' % (100 - t * 100 / 30))
                self.progressBar.setValue(t1)
                if t == 0:
                    self.measure_flag = False
                    self.progressBar.setValue(0)
                    QMessageBox.information(self, '提示', '测量完成', QMessageBox.Yes)
                    self.close()


