from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from common.libs.Log import logging

class TaskRunManager(QLabel):
    signal = pyqtSignal(str)
    def __init__(self, parent = None):
        super(TaskRunManager, self).__init__(parent)
        # 标志
        self.flag = False
        # 任务
        self.task_step = 0
        self.task_list = []
        self.task_current = None
        # 执行步骤
        self.run_step = ""
        self.run_flag = False
        # 定时器
        self.timer = QTimer()
        self.timer.timeout.connect(self.running)
        self.timer.start(100)

    def startMove(self, tasks):
        logging.info("开始执行任务:")
        logging.info(tasks)
        self.task_list = tasks
        if self.task_list:
            self.flag = True
            self.run_flag = True
            self.run_step = "start"
        else:
            QMessageBox.warning(self, "提示","请选择要执行的任务！", QMessageBox.Yes)

    def stopMove(self):
        if self.task_current:
            self.run_flag = True
            self.run_step = "stop"
        else:
            QMessageBox.warning(self, "提示","当前无任务运行", QMessageBox.Yes)

    def processMove(self):
        self.run_flag = True
        self.run_step = "process"

    def nextMove(self):
        self.task_step += 1
        self.run_flag = True
        self.run_step = "start"

    def running(self):
        # if not self.done:
        if self.run_flag:
            self.run_flag = False
            if self.run_step == "start": # 移动到当前任务起始位置
                self.task_current = self.task_list[self.task_step]
                self.signal.emit("start")
            if self.run_step == "process": # 继续
                self.signal.emit("process")
            if self.run_step == "stop": # 停止
                self.signal.emit("stop")
    
    def checkDone(self):
        """检测所有任务是否执行完成"""
        if self.task_step >= len(self.task_list) - 1:
            return True
        else:
            return False

    def clearFlag(self):
        "清空标志"
        self.flag = False
        # 任务
        self.task_step = 0
        self.task_list = []
        self.task_current = None
        # 执行步骤
        self.run_step = ""
        self.run_flag = False

# class TaskRunManager(QLabel):
#     signal = pyqtSignal(str)
#     def __init__(self, parent = None):
#         super(TaskRunManager, self).__init__(parent)
#         # Thread.__init__(self)
#         # self.setDaemon(True)
#         # 消息推送堆栈
#         # self.msg_queue = msg_queue
#         # 线程开关
#         self.done = False
#         # 执行进度
#         self.step = 0
#         # 任务运行标志
#         self.task_running = False
#         # 开始运行一次标志
#         self.start_run = False
#         # 暂停标志
#         self.pause_run = False
#         self.pause_flag = False
#         # 继续标志
#         self.continue_run = False
#         # 下一个标志
#         self.next_run = False
#         # 执行任务第一个点
#         self.first_pos_run = False
#         # 定位执行标志
#         self.select_run = False
#         self.first_run = False
#         # 移动到第一个点标志
#         self.move_to_first_flag = False
#         # 移动边测量标志
#         self.move_measure_flag = False

#     def running(self):
#         if not self.done:
#             if self.start_run:  # 执行一次
#                 self.start_run = False
#                 if self.pause_run:  # 暂停
#                     self.pause_run = False
#                     # 标记暂停
#                     self.pause_flag = True
#                     self.signal.emit("PAUSE")
#                     # 关闭移动到第一个点标志
#                     self.move_to_first_flag = False
#                 else:
#                     self.task_running = True
#                     # if self.step < len(self.task):
#                     if self.continue_run:  # 继续执行
#                         self.continue_run = False
#                         self.signal.emit("CONTINUE")
#                     if self.select_run:  # 选择位置处执行任务
#                         self.select_run = False
#                         self.signal.emit("SELECT")
#                     if self.first_run:  # 第一个任务开始执行
#                         self.first_run = False
#                         self.signal.emit("FIRST")
#                     if self.next_run:  # 执行下一个任务
#                         self.next_run = False
#                         self.signal.emit("NEXT")
#                     if self.first_pos_run:  # 移动到第一个点
#                         self.first_pos_run = False
#                         self.signal.emit("MOVE_FIRST")
#                         self.move_to_first_flag = True

#     def startRun(self):
#         " 开始执行"
#         self.start_run = True

#     def firstRun(self):
#         "第一个位置处执行"
#         self.start_run = True
#         self.first_run = True

#     def selectRun(self):
#         "选择处执行"
#         self.start_run = True
#         self.select_run = True

#     def pauseRun(self):
#         "暂停"
#         self.start_run = True
#         self.pause_run = True

#     def continueRun(self):
#         "继续"
#         self.start_run = True
#         self.continue_run = True

#     def moveToFirstPos(self):
#         "移动到第一个点位置"
#         self.start_run = True
#         self.first_pos_run = True

#     def nextRun(self):
#         "开始下一个任务"
#         self.start_run = True
#         self.next_run = True
#         self.step += 1

#     def clearFlag(self):
#         "清空标志"
#         self.step = 0
#         self.task_running = False
#         self.start_run = False
#         self.pause_run = False
#         self.pause_flag = False
#         self.continue_run = False
#         self.next_run = False
#         self.first_pos_run = False
#         self.select_run = False
#         self.first_run = False
#         self.move_to_first_flag = False
#         self.move_measure_flag = False

