#!/usr/bin/env python
# encoding: utf-8

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

from ui.sameDose import Ui_Dialog as sameDoseDialog
from win32api import GetSystemMetrics
from .ChartDeal import ChartDealWidget, tabView, GraphWidget

# import re
# import os
# import math
import random
import numpy as np

import pyqtgraph as pg
import pyqtgraph.opengl as gl
from pyqtgraph.opengl import GLViewWidget
from pyqtgraph import GraphicsLayoutWidget

from scipy.interpolate import interp1d
from scipy import interpolate


# pg.setConfigOption('background', '#000000')
# pg.setConfigOption('foreground', 'k')
colorlist = [
    (255, 0, 0),
    (255, 0, 0),
    (255, 128, 0),
    (255, 255, 0),
    (0, 250, 0),
    (128, 128, 192),
    (255, 0, 255),
    (0, 128, 128),
    (128, 255, 255),
    (0, 128, 255),
    (0, 0, 255),
    (0, 0, 128),
]


class TabFile(QTabWidget):
    def __init__(self, parent=None):
        super(TabFile, self).__init__(parent)

        self.tab_chart = GraphWidget()
        self.tab_data = tabView()

        self.addTab(self.tab_chart, "表格")
        self.addTab(self.tab_data, "数据")

        self.tabChartUI()
        self.tabDataUI()

    def tabChartUI(self):
        layout = QHBoxLayout()
        self.setTabText(0, "表格")
        self.tab_chart.setLayout(layout)

    def tabDataUI(self):
        layout = QHBoxLayout()
        self.setTabText(1, "数据")
        self.tab_data.setLayout(layout)


class SameGraphWidget3D(GLViewWidget):
    # 3d图中
    # 网格控件: 1.中心为0点; 2.最小单位为1; 3.网格宽度默认为20;
    def __init__(self):
        super(SameGraphWidget3D, self).__init__()
        self.opts['distance'] = 40
        # self.opts['elevation'] = 0
        # self.opts['azimuth'] = 0

        # x网格平面, 底面
        self.gx = gl.GLGridItem()
        self.gx.translate(0, 0, -10)
        self.addItem(self.gx)

        # y网格平面, 右侧
        self.gy = gl.GLGridItem()
        self.gy.rotate(90, 0, 1, 0)
        self.gy.translate(-10, 0, 0)
        self.addItem(self.gy)

        # z网格平面, 左侧
        self.gz = gl.GLGridItem()
        self.gz.rotate(90, 1, 0, 0)
        self.gz.translate(0, -10, 0)
        self.addItem(self.gz)

        # 范围
        self.x_range = [-100, 100]
        self.y_range = [0, 100]
        self.z_range = [0, 300]

    def axisToGL(self, x, y, z):
        '''按比例返回opengl中xyz坐标'''
        x = x*self.x_scale/2
        y = y*self.y_scale/2
        z = (self.z_range/2-z)*self.z_scale/2
        return x, y, z

    def drawSameSurface(self, ray_size=10, data=[]):
        # data = [self.axisToGL(d[0], d[1], d[2]) for d in data]
        self.points = [[r[0], r[1], r[2]] for r in data]
        color = [r[3] for r in data]
        color = [(c[0]/255, c[1]/255, c[2]/255, 0.5) for c in color]
        size = [0.1 for r in data]
        # print(color)
        self.sp = gl.GLScatterPlotItem(pos=np.array(
            self.points), size=np.array(size), color=np.array(color), pxMode=False)
        self.sp.translate(0, -10, 10)

        self.scale_x = 20/(ray_size*10)/2
        self.scale_y = 20/100
        self.scale_z = 20/300

        self.x_range = [-int(ray_size*10), int(ray_size*10)]

        self.sp.scale(self.scale_x, self.scale_y, -self.scale_z)
        self.addItem(self.sp)

    def drawSameLines(self, ray_size=10, data=[]):
        self.scale_x = 20/(ray_size*10)/2
        self.scale_y = 20/100
        self.scale_z = 20/300
        self.x_range = [-int(ray_size*10), int(ray_size*10)]
        # print(data)
        i = 0
        for ln in data:
            i = i+1
            x = np.array([t[0] for t in ln])
            y = np.array([t[1] for t in ln])
            z = np.array([t[2] for t in ln])
            pts = np.vstack([x, y, z]).transpose()
            plt = gl.GLLinePlotItem(pos=pts, color=pg.glColor(colorlist[int(i/20)]), width=1, antialias=True)
            plt.translate(0, -10, 10)
            plt.scale(self.scale_x, self.scale_y, -self.scale_z)
            self.addItem(plt)
        # self.addItem(self.sp)

    def paintGL(self, *args, **kwds):
        GLViewWidget.paintGL(self, *args, **kwds)
        textFont = QFont()
        textFont.setPixelSize(12)
        self.qglColor(Qt.white)
        for i in range(self.x_range[0], self.x_range[1]+1):
            step = int((self.x_range[1]-self.x_range[0]) / 10)
            if i % step == 0:
                self.renderText(i*self.scale_x, 11, -10, '%.0f' % i, textFont)
        for i in range(self.y_range[0], self.y_range[1]+1):
            step = int((self.y_range[1]-self.y_range[0]) / 10)
            if i % step == 0:
                self.renderText(11, i*self.scale_y-10, -
                                10, '%.0f' % i, textFont)
        for i in range(self.z_range[0], self.z_range[1]+1):
            step = int((self.z_range[1]-self.z_range[0]) / 10)
            if i % step == 0:
                self.renderText(-11, 11, -(i*self.scale_z-10),
                                '%.0f' % i, textFont)
        self.renderText(0, 0, 12, '不同深度百分比示意图')
        self.qglColor(Qt.blue)
        textFont.setPixelSize(15)
        self.renderText(0, 12, -10, 'x(mm)', textFont)
        self.renderText(12, 0, -10, 'rate(%)', textFont)
        self.renderText(-11, 12, 0, 'deepth(mm)', textFont)


class SameGraphWidget(GraphicsLayoutWidget):
    pos_x = [0, 1, 2, 3, 4, 5]
    pos_y = [0, 0, 0, 0, 0, 0]
    conf = None
    original_x = [0, 1, 2, 3, 4, 5]
    original_y = [0, 0, 0, 0, 0, 0]
    signal_result = pyqtSignal(str)
    signal_result_update = pyqtSignal(dict)
    result = {}

    pos_x_l = [0, 1, 2, 3, 4, 5]
    pos_y_l = [5, 5, 5, 5, 5, 5]
    pos_x_r = [0, 1, 2, 3, 4, 5]
    pos_y_r = [-5, 5, 5, 5, 5, 5]

    def __init__(self):
        super(SameGraphWidget, self).__init__()
        # 添加文本显示坐标等信息
        self.label = pg.LabelItem(justify='right')
        self.addItem(self.label)
        # 添加图表plot
        self.plot = self.addPlot(row=1, col=0, colspan=2)
        # 图表标题设置
        self.plot.setTitle('曲线')
        # 图表显示字体设置
        font = QFont()
        font.setBold(True)
        font.setPixelSize(15)
        self.plot.getAxis("bottom").tickFont = font
        self.plot.getAxis("left").tickFont = font
        self.plot.setAutoVisible(y=True)

        self.legends = pg.LegendItem()
        self.legends.setParentItem(self.plot)

        # 图表网格显示
        self.plot.showGrid(x=True, y=True, alpha=0.7)

        # 图表坐标显示
        self.plot.setLabel('left', "深度(mm)", units='')
        self.plot.setLabel('bottom', "距离(mm)", units='')
        self.plot.setLimits(xMin=-200, xMax=200, yMin=-300, yMax=10)
        # # 添加曲线
        self.curve_rate_l = self.plot.plot(
            self.pos_x_l, self.pos_y_l, pen=pg.mkPen('r', width=4), name='曲线')
        self.curve_rate_r = self.plot.plot(
            self.pos_x_l, self.pos_y_l, pen=pg.mkPen('r', width=4), name='曲线')
        # self.plot.addItem(self.curve_rate_l)
        # self.plot.addItem(self.curve_rate_r)
        # self.curve_r = self.plot.plot(
        #     self.pos_x_l, self.pos_y_l, pen=pg.mkPen('b', width=2), name='测量曲线')

        # # 数据点显示
        # self.scatter_l = pg.ScatterPlotItem(
        #     size=10, pen=pg.mkPen(None), brush=pg.mkBrush(255, 0, 0, 180))
        # self.scatter_l.setData(self.pos_x_l, self.pos_y_l)
        # self.plot.addItem(self.scatter_l)

        # self.scatter_r = pg.ScatterPlotItem(
        #     size=10, pen=pg.mkPen(None), brush=pg.mkBrush(0, 0, 255, 180))
        # self.scatter_r.setData(self.pos_x_r, self.pos_y_r)
        # self.plot.addItem(self.scatter_r)

        # # 实时位置点显示
        # self.pos_scatter_l = pg.ScatterPlotItem(
        #     [0], [0], size=20, pen=pg.mkPen(None), brush=pg.mkBrush(250, 0, 0, 250))
        # self.plot.addItem(self.pos_scatter_l)

        # self.pos_scatter_r = pg.ScatterPlotItem(
        #     [0], [0], size=20, pen=pg.mkPen(None), brush=pg.mkBrush(0, 0, 250, 250))
        # self.plot.addItem(self.pos_scatter_r)

        # 鼠标十字移动
        self.vLine = pg.InfiniteLine(pen=pg.mkPen(
            'b', width=1), angle=90, movable=False)
        self.hLine = pg.InfiniteLine(pen=pg.mkPen(
            'b', width=1), angle=0, movable=False)
        self.plot.addItem(self.vLine, ignoreBounds=True)
        self.plot.addItem(self.hLine, ignoreBounds=True)
        # 鼠标移动绑定
        self.vb = self.plot.vb
        self.proxy = pg.SignalProxy(
            self.plot.scene().sigMouseMoved, rateLimit=60, slot=self.mouseMoved)

    def np_find_nearest(self, array, value):
        idx = (np.abs(array - value)).argmin()
        return idx

    def mouseMoved(self, evt):
        pos = evt[0]  # using signal proxy turns original arguments into a tuple
        if self.plot.sceneBoundingRect().contains(pos):
            try:
                mousePoint = self.vb.mapSceneToView(pos)
                index = mousePoint.x()
                x = mousePoint.x()
                y = mousePoint.y()
                self.setCursor(Qt.BlankCursor)
                self.vLine.setPos(x)
                self.hLine.setPos(y)

                # idx = self.np_find_nearest(np.array(self.pos_x_l), x)
                # pos_x_l = self.pos_x_l[idx]
                # pos_y_l = self.pos_y_l[idx]
                # self.pos_scatter_l.setData([pos_x_l], [pos_y_l])

                # idx = self.np_find_nearest(np.array(self.pos_x_r), x)
                # pos_x_r = self.pos_x_r[idx]
                # pos_y_r = self.pos_y_r[idx]
                # self.pos_scatter_r.setData([pos_x_r], [pos_y_r])

                txt_mouse = '''<span style='font-size: 12pt'>x=%0.1f, <span style='color: red'>y=%0.1f</span>''' % (
                    x, y)
                # txt_pos_l = '''<span style='font-size: 12pt'>x=%0.1f, <span style='color: red'>y=%0.1f</span>''' % (
                #     pos_x_l, pos_y_l)
                # txt_pos_r = '''<span style='font-size: 12pt'>x=%0.1f, <span style='color: blue'>y=%0.1f</span>''' % (
                #     pos_x_r, pos_y_r)
                self.label.setText("移动坐标:%s" % txt_mouse)
                # self.label.setText("移动坐标:%s, 左侧点位坐标:%s, 右侧点位坐标:%s" %
                #                    (txt_mouse, txt_pos_l, txt_pos_r))
            except:
                pass

    def drawRateLine(self, line):
        self.plot.clear()
        line_left = line['left']
        line_right = line['right']
        x_l = np.array(line_left['x'])
        y_l = np.array(line_left['y'])
        self.curve_rate_l.setData(x_l, y_l)
        x_r = np.array(line_right['x'])
        y_r = np.array(line_right['y'])
        self.curve_rate_r.setData(x_r, y_r)

        self.plot.addItem(self.vLine, ignoreBounds=True)
        self.plot.addItem(self.hLine, ignoreBounds=True)
        self.plot.addItem(self.curve_rate_l, ignoreBounds=True)
        self.plot.addItem(self.curve_rate_r, ignoreBounds=True)
        for line in self.cruves:
            self.plot.addItem(line, ignoreBounds=True)

    def multiLine(self, lines):
        '''多条曲线显示'''
        self.plot.clear()
        for sample, label in self.legends.items:
            self.legends.removeItem(label.text)
        self.cruves = []
        self.pos_scatters = []
        for item in lines:
            left = item['left']
            x_l = np.array(left['x'])
            y_l = np.array(left['y'])
            curve_left = self.plot.plot(x_l, y_l, pen=pg.mkPen(item['color']), width=3)
            self.cruves.append(curve_left)

            right = item['right']
            x_r = np.array(right['x'])
            y_r = np.array(right['y'])
            curve_right = self.plot.plot(x_r, y_r, pen=pg.mkPen(item['color']), width=3)
            self.cruves.append(curve_right)
            self.legends.addItem(curve_left, '%.3f%%' % item['rate'])
        self.plot.addItem(self.vLine, ignoreBounds=True)
        self.plot.addItem(self.hLine, ignoreBounds=True)
        self.plot.addItem(self.curve_rate_l, ignoreBounds=True)
        self.plot.addItem(self.curve_rate_r, ignoreBounds=True)
        return


class SameDoseDialog(QDialog, sameDoseDialog):
    def __init__(self, parent=None):
        super(QDialog, self).__init__(parent)
        self.setupUi(self)

        #  窗口尺寸设计
        w = GetSystemMetrics(0)/1.1
        h = GetSystemMetrics(1)/1.1
        self.setFixedSize(w, h)
        self.setWindowTitle("最大吸收剂量比")
        self.pushButton_getResult.clicked.connect(self.getResult)
        self.pushButton_getRate.clicked.connect(self.rateChanged)
        # self.doubleSpinBox.valueChanged.connect(self.rateChanged)
        # self.graphWidget = GraphWidget()
        self.graphWidget = SameGraphWidget()
        self.graphWidget3d = SameGraphWidget3D()
        # self.verticalLayout_line.addWidget(self.graphWidget)
        self.verticalLayout_3d.addWidget(self.graphWidget3d)
        # self.graphWidget.updatePos(x, normal, conf)

        self.tab_same = QTabWidget()
        # 动态变化表格
        self.change_table = tabView()
        self.change_table.HeaderList = ['x(mm)', 'd(mm)']
        self.change_table.clear_data()

        # self.table_list = []
        # for i in range(9):
        #     rate = i * 10
        #     table = tabView()
        #     table.HeaderList = ['x(mm)', 'd(mm)']
        #     table.clear_data()
        #     self.table_list.append(table)
        #     self.tab_same.addTab(table, str('表(%s%%)' % rate))
        self.verticalLayout_same.addWidget(self.tab_same)
    
    def rateChanged(self):
        lines = self.lines
        rate = self.doubleSpinBox.value()
        line = self.getSameRateLines(lines, rate)
        data_left = [[x, y] for x, y in zip(line['left']['x'][::10], line['left']['y'][::10])]
        data_right = [[x, y] for x, y in zip(line['right']['x'][::10], line['right']['y'][::10])]
        data = data_left + data_right
        self.change_table.addDataRow(data)
        self.graphWidget.drawRateLine(line)

    def aveFilter(self, y, span):
        '''均值滤波
        span = 5:
        y(1) = y(1)
        y(2) = y(1)+y(2)+y(3)/3
        y(3) = y(1)+y(2)+y(3)+y(4)+y(5)/5
        '''
        # span = 7
        s = 0
        cen = int(span/2)
        for i in range(0, len(y)):
            if i >= cen:
                y_t = y[i-cen:i+cen]
                if len(y)-i <= cen:
                    y[i] = y[i]
                else:
                    y[i] = sum(y_t)/len(y_t)
            elif 0 < i < cen:
                s = s+2
                y_t = y[0:s+1]
                y[i] = sum(y_t)/len(y_t)
        return y

    def getResult(self):
        def linearInterpolation(x, y, rate):
            '''插值法计算'''
            for i in range(1, len(y)):
                if y[i-1] <= rate < y[i] or y[i-1] >= rate > y[i]:
                    k = (y[i] - y[i-1])/(x[i] - x[i-1])
                    b = y[i] - k*x[i]
                    x_r = (rate - b)/k
                    return x_r
            return False

        def find_nearest(array, value):
            array = np.array(array)
            res = array.flat[np.abs(array - value).argmin()]
            if res:
                return np.where(array == res)[0][0]
            return False

        info = self.info
        max_pos = 0
        x_r = []
        y_r = []
        ray_size = 0
        # 找最大剂量点
        for item in info:
            data = item['data']
            conf = item['conf']
            if conf['type'] == 'PDD':
                ray_size = float(conf['ray_size_x'].replace('cm', ''))
                x = [float(item[2]) for item in data]
                pdd_noraml = [float(item[5]) for item in data]
                y = [float(item[6]) for item in data]
                # max_doserate = max(y)
                max_pos = y.index(max(y))
                # max_dose_deepth = x[max_pos]
                # print(max_pos)
                # 处理pdd最大值右边区域
                x_r = x[max_pos:]
                y_r = y[max_pos:]
                # 延长pdd曲线，构建到最小10%
                err_y = y_r[len(y_r)-1] - y_r[len(y_r)-2]
                err_x = x_r[len(x_r)-1] - x_r[len(x_r)-2]
                k = err_y / err_x
                b = y_r[len(y_r)-1] - k*x_r[len(x_r)-1]
                x_r.append((9 - b)/k)
                y_r.append(9)
                break

        # 所有oar按照最大剂量点归一化
        new_data = []
        for item in info:
            data = item['data']
            conf = item['conf']
            if conf['type'] == 'OAR':
                deepth = float(conf['deepth'].replace('mm', ''))
                x = [float(d[0]) for d in data]
                y = [float(d[1]) for d in data]
                noraml = [float('%.3f' % float(d[5])) for d in data]
                normal = [((float('%.4f' % (100*n/pdd_noraml[max_pos]))))
                          for n in noraml]
                center_rate = normal[int(len(noraml)/2)]
                # 按深度转换
                # normal = [-deepth*n/center_rate for n in normal]
                if 'angle' in conf:
                    if conf['angle'] == '0' or conf['angle'] == '180':
                        if conf['direction'] == 'G->T' or conf['direction'] == 'T->G':
                            x = y
                        else:
                            x = x
                    if conf['angle'] == '90' or conf['angle'] == '270':
                        if conf['direction'] == 'G->T' or conf['direction'] == 'T->G':
                            x = x
                        else:
                            x = y
                else:
                    if conf['direction'] == 'G->T' or conf['direction'] == 'T->G':
                        x = y
                new_data.append({
                    "color": colorlist[random.randint(0, 10)],
                    "deepth": deepth,
                    "rate": center_rate,
                    "conf": conf,
                    "data": {
                        "x": x,
                        "y": normal,
                    }
                })
        # 按照深度排序
        sorted(new_data, key=lambda i: i['deepth'])
        # 拟合制作不同深度oar百分比数据
        deepth_arr = [d['deepth'] for d in new_data]
        points = []
        lines = []
        for r in range(0, 360):
            rate = 100-r/4
            deepth = linearInterpolation(x_r, y_r, rate)
            idx = find_nearest(deepth_arr, deepth)
            item = new_data[idx]
            item_rate = item['rate']
            # 按比例生成oar拟合曲线
            x = item['data']['x']
            y = [t*rate/item_rate for t in item['data']['y']]
            # 曲线平滑补点
            x = np.array(x)
            y = np.array(y)
            new_x = np.arange(min(x), max(x), 0.1)
            # new_x = np.arange(-200, 200, 0.2)
            func = interp1d(x, y, kind='linear')
            # func = interp1d(x, y, kind='quadratic')
            new_y = func(new_x)
            # 生成深度曲线
            if deepth <= 300:
                tmp = []
                for m, n in zip(new_x, new_y):
                    points.append([
                        float('%.3f' % m),
                        float('%.3f' % n),
                        float('%.3f' % deepth)])
                    tmp.append([
                        float('%.3f' % m),
                        float('%.3f' % n),
                        float('%.3f' % deepth)])
                lines.append(tmp)
        self.lines = lines

        # 3d点位添加颜色
        color_points = []
        for p in points:
            for r in range(0, 11):
                rate = r*10
                if rate <= p[1] < rate + 10:
                    # p[1] = 0
                    p.append(colorlist[10-r])
                    color_points.append(p)
        self.graphWidget3d.drawSameSurface(
            ray_size=ray_size, data=color_points)

        # 筛选等剂量线
        same_lines = []
        for r in range(1, 10):
            rate = r * 10
            line = self.getSameRateLines(lines, rate)
            line['color'] = colorlist[10-r]
            same_lines.append(line)
        self.graphWidget.multiLine(same_lines)
        self.sameTable(same_lines)
        self.rateChanged()

    def getSameRateLines(self, lines, rate):
        # 筛选等剂量线, 分三份
        sp_lines = [[], [], []]
        u = [1, self.doubleSpinBox_unit.value(), 1]
        s = sum(u)
        for ln in lines:
            length = len(ln)
            step1 = int(length*u[0]/s)
            step2 = int(length*(u[0]+u[1])/s)
            sp_lines[0].append(ln[:step1])
            sp_lines[1].append(ln[step1:step2])
            sp_lines[2].append(ln[step2:])

        # print(sp_lines[0])
        line = []
        idx = 0
        for idx in range(len(sp_lines)):
            sp = sp_lines[idx]
            tmp = []
            for ln in sp:
                x = [t[0] for t in ln]
                y = [t[1] for t in ln]
                d = [t[2] for t in ln]
                # 曲线整体小于当前等剂量线，不用查找
                if max(y) <= rate:
                    continue
                if min(y) >= rate:
                    continue

                for i in range(len(y)):
                    if abs(y[i] - rate) < 0.1:
                        tmp.append([x[i], -d[i], colorlist[int(rate/10)]])
            if idx == 1:
                tmp = sorted(tmp, key=lambda x: x[0])
            if idx == 2:
                tmp = tmp[::-1]
            line = line + tmp

        def filter(arr, step):
            new = []
            for i in range(len(arr)):
                if i % step == 0:
                    new.append(arr[i])
            return new

        # line = filter(line, 3)
        line_l = []
        line_r = []
        for i in range(len(line)):
            ln = line[i]
            if ln[0] < 0:
                line_l.append(ln)
            if ln[0] > 0:
                line_r.append(ln)

        x_l = [d[0] for d in line_l]
        y_l = [d[1] for d in line_l]

        end = x_l[-1]
        x_l = x_l[:-1][::10]
        x_l.append(end)

        end = y_l[-1]
        y_l = y_l[:-1][::10]
        y_l.append(end)

        # y_l = self.aveFilter(y_l, 7)

        x_r = [d[0] for d in line_r]
        y_r = [d[1] for d in line_r]

        end = x_r[-1]
        x_r = x_r[:-1][::10]
        x_r.append(end)

        end = y_r[-1]
        y_r = y_r[:-1][::10]
        y_r.append(end)

        # y_r = self.aveFilter(y_r, 7)

        # 滤波
        # line = filter(line, 3)
        # x = [d[0] for d in line]
        # y = [d[1] for d in line]
        # x = self.aveFilter(x, 7)
        # y = self.aveFilter(y, 7)
        # p = int(len(x) / 2)
        res = {
            "rate": rate,
            "left": {
                # 'x': x[:p],
                # 'y': y[:p],
                'x': x_l,
                'y': y_l,
            },
            "right": {
                'x': x_r,
                'y': y_r,
                # 'x': x[p:],
                # 'y': y[p:],
            },
        }
        return res

    def sameTable(self, lines):
        self.tab_same.clear()
        self.tab_same.addTab(self.graphWidget, "曲线")
        self.tab_same.addTab(self.change_table, str('表(设置)'))
        # 其他表格数据
        for i in range(len(lines)):
            ln = lines[i]
            data_left = [[x, y] for x, y in zip(ln['left']['x'][::10], ln['left']['y'][::10])]
            data_right = [[x, y] for x, y in zip(ln['right']['x'][::10], ln['right']['y'][::10])]
            data = data_left + data_right
            table = tabView()
            table.HeaderList = ['x(mm)', 'd(mm)']
            table.clear_data()
            table.addDataRow(data)
            self.tab_same.addTab(table, str('表(%s%%)' % lines[i]['rate']))

    def setFiles(self, file_list):
        tab = QTabWidget()
        info = []
        for i in range(len(file_list)):
            f = file_list[i]
            chartDealDlg = ChartDealWidget()
            chartDealDlg.showOneLine(f)
            tab.addTab(chartDealDlg, str(f.split('/')[-1]))
            info.append({
                "data": chartDealDlg.table.get_data_by_row(),
                "conf": chartDealDlg.conf,
            })
        self.horizontalLayout.addWidget(tab)
        self.info = info
        self.getResult()


if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    win = OarDialog()
    win.show()
    app.exec_()
