#!/usr/bin/env python
# encoding: utf-8

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from pyqtgraph import GraphicsLayoutWidget
import pyqtgraph as pg
# pg.setConfigOption('background', '#E3E6E3')
# pg.setConfigOption('foreground', 'k')

import random
from win32api import GetSystemMetrics

class GraphWindow(GraphicsLayoutWidget):
    def __init__(self):
        super(GraphWindow, self).__init__()
        font=QFont()
        font.setBold(True)
        font.setPixelSize(15)
        self.label = pg.LabelItem(justify='right')
        self.label.setFont(font)
        self.addItem(self.label,0,1,colspan=1)
        self.tunnelLine = self.addPlot(1,1,colspan=2)
        self.tunnelLine.getAxis("bottom").tickFont = font
        self.tunnelLine.getAxis("left").tickFont = font
        self.tunnelLine.setAutoVisible(y=True)
        self.tunnelLine.setTitle('曲线')
        self.text = pg.TextItem(html='<div style="text-align: center"><span style="color: #FF0;">information</span><br><span style="color: #FFF; font-size: 26pt;">PEAK</span></div>', anchor=(-0.3,0.5), angle=0, border='w', fill=(0, 0, 255, 100))
        self.tunnelLine.addItem(self.text)
        self.text.setPos(200,100)

        self.colorlist = [
            (0,128,64),
            (0,0,255),
            (0,0,160),
            (128,0,128),
            (128,0,255),
            (255,128,0),
            (255,128,128),
            (255,128,255),
            (255,128,192),
            (128,0,0), ]

        #鼠标十字移动
        self.vLine = pg.InfiniteLine(pen=pg.mkPen('b', width=1),angle=90, movable=False)
        self.hLine = pg.InfiniteLine(pen=pg.mkPen('b', width=1),angle=0, movable=False)
        self.tunnelLine.addItem(self.vLine, ignoreBounds=True)
        self.tunnelLine.addItem(self.hLine, ignoreBounds=True)
        #鼠标移动绑定
        self.vb = self.tunnelLine.vb
        self.proxy = pg.SignalProxy(self.tunnelLine.scene().sigMouseMoved, rateLimit=60, slot=self.mouseMoved)
        # 鼠标坐标显示文本
        self.text_pos = pg.TextItem("(0,0)", anchor=(0.5, -1.0),color='b')
        self.tunnelLine.addItem(self.text_pos)
        self.text_pos.setPos(0,0)
        # 网格显示
        self.tunnelLine.showGrid(x = True, y = True, alpha = 0.7)
        # 坐标显示
        self.tunnelLine.setLabel('left', "相对剂量(%)", units='')
        self.tunnelLine.setLabel('bottom', "距离(mm)", units='')

    def update(self):
        self.data[:-1] = self.data[1:]  # shift data in the array one sample left
                                # (see also: np.roll)
        self.data[-1] = np.random.normal()

    def updateRegion(self, window, viewRange):
        rgn = viewRange[0]
        self.region.setRegion(rgn)

    def mouseMoved(self, evt):
        pos = evt[0]  ## using signal proxy turns original arguments into a tuple
        if self.tunnelLine.sceneBoundingRect().contains(pos):
            mousePoint = self.vb.mapSceneToView(pos)
            index = int(mousePoint.x())
            # self.tunnelLine.setLabel('bottom', "距离(mm)\t\t\t\tx=%s"%mousePoint.x(), units='')
            # self.setCursor(Qt.BlankCursor)

            # self.text_pos.setText("(%0.1f,%0.1f)"%(mousePoint.x(),mousePoint.y()))
            # self.text_pos.setPos(mousePoint.x(),mousePoint.y())
            x = mousePoint.x()
            y = mousePoint.y()
            self.vLine.setPos(x)
            self.hLine.setPos(y)
            txt_mouse = '''<span style='font-size: 12pt'>x=%0.1f, <span style='color: red'>y=%0.1f</span>''' % (x, y)
            self.label.setText("移动坐标:%s"%txt_mouse)

    def multiLine(self,x,y,c):
        '''多条曲线显示'''
        # self.tunnelLine.clear()
        self.legends = pg.LegendItem()
        self.legends.setParentItem(self.tunnelLine)
        for i in range(0,len(x)):
            curve= self.tunnelLine.plot(x[i],y[i],pen=pg.mkPen(color=(random.randint(0,255),random.randint(0,255),random.randint(0,255),),width=3))
            self.legends.addItem(curve,c[i])
        self.tunnelLine.addItem(self.vLine, ignoreBounds=True)
        self.tunnelLine.addItem(self.hLine, ignoreBounds=True)
        self.tunnelLine.addItem(self.text_pos)
        return

from ui.multiLineAnalyse import Ui_Dialog
class MultiLineDialog(QDialog, Ui_Dialog):
    def __init__(self,parent=None):
        super(QDialog, self).__init__(parent)
        self.setupUi(self)

        w = GetSystemMetrics (0)/1.2
        h = GetSystemMetrics (1)/1.2
        self.setFixedSize(w,h)

        self.chart = GraphWindow()
        self.verticalLayout_chart.addWidget(self.chart)

if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    window = MultiLineDialog()
    window.show()
    window.resize(1266, 768)
    app.exec_()

