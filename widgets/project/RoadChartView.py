#!/usr/bin/env python
# encoding: utf-8

from PyQt5.QtCore import *
from pyqtgraph import GraphicsLayoutWidget
import pyqtgraph as pg
import numpy as np

class RoadChartView(GraphicsLayoutWidget):
    timer_cnt = 0
    pdd_oar = ''
    road_step = []
    def __init__(self):
        super(RoadChartView, self).__init__()

        self.label = pg.LabelItem(justify='right')
        self.addItem(self.label,0,1,colspan=2)
        self.plot = self.addPlot(1,1,colspan=2)
        # self.curve_x = self.plot.plot([-300,0,300],[0,0,0], pen=(0,200,200), symbolBrush=(0,0,200), symbolPen='w', symbol='o', symbolSize=8, name="symbol='o'")
        self.plot.setAutoVisible(y=True)
        self.curveRoad = self.plot.plot([0,0],[0,0],pen=(0,0,200))
        self.curveLine = self.plot.plot([0,0],[0,0],pen=(0,0,200))
        self.curveZero = self.plot.plot([0,0],[0,10],pen=(200,0,0))

        self.timer_move = QTimer()
        self.timer_move.timeout.connect(self.roadMove)
        self.timer_move.start(500)

        self.curveRoadPoint = pg.CurvePoint(self.curveRoad)
        self.plot.addItem(self.curveRoadPoint)
        self.moveText = pg.TextItem("test", anchor=(0.5, -1.0))
        self.moveText.setParentItem(self.curveRoadPoint)
        self.moveArrow = pg.ArrowItem(angle=90)
        self.moveArrow.setParentItem(self.curveRoadPoint)

        self.road = []

    def updatePddRoad(self, step, pos, road, direction):
        self.road = road
        self.road_step = [r[2] for r in road]
        pos_road = np.array([[r[2],0] for r in road])
        self.curveRoad.setData(np.array(pos_road))

        if direction == 'G->O':
            road = road[::-1]
        pos_arr = []
        len_arr = [10,20,30,40,50]
        for i in range(len(pos)-1):
            for j in range(len(road)-1):
                r = road[j]
                r_n = road[j+1]
                if pos[i] <= r[2] < pos[i+1]:
                    pos_arr.append([r[2], 0])
                    pos_arr.append([r[2], (i+1)*10])
                    pos_arr.append([r_n[2], (i+1)*10])
                # 最后一个点位
                if r_n[2] == pos[i+1] and j == len(road) - 2:
                    pos_arr.append([r_n[2], 0])
        self.curveLine.setData(np.array(pos_arr))

        self.plot.setXRange(pos[0]-10, pos[-1]+10, padding=0)
        self.plot.setYRange(-20, len(pos)*10, padding=0)
        self.plot.showGrid(x = False, y = False, alpha = 0.3)

    def updateOarRoad(self, step, pos, road, direction, half, angle):
        self.road = road
        # 取右半边路径进行计算
        if angle == 90 or angle == 270:
            if direction == 'A->B' or direction == 'B->A':
                self.road_step = [r[1] for r in road]
                pos_road = np.array([[r[1],0] for r in road])
            else:
                self.road_step = [r[0] for r in road]
                pos_road = np.array([[r[0],0] for r in road])
        if angle == 0 or angle == 180:
            if direction == 'G->T' or direction == 'T->G':
                self.road_step = [r[1] for r in road]
                pos_road = np.array([[r[1],0] for r in road])
            else:
                self.road_step = [r[0] for r in road]
                pos_road = np.array([[r[0],0] for r in road])
        road = self.road_step
        line_road = []
        for j in range(len(road)-1):
            for i in range(len(pos)-1):
                ok = False
                if road[0] < 0:  # 负->正
                    if road[j] < 0:
                        if pos[i] < abs(road[j]) <= pos[i+1]:
                            ok = True
                    if road[j] > 0:
                        if pos[i] <= abs(road[j]) < pos[i+1]:
                            ok = True

                if road[0] > 0:  # 正->负
                    if road[j] < 0:
                        if pos[i] <= abs(road[j]) < pos[i+1]:
                            ok = True
                    if road[j] > 0:
                        if pos[i] < abs(road[j]) <= pos[i+1]:
                            ok = True
                if ok:
                    line_road.append([road[j], 0])
                    line_road.append([road[j], (i+1)*10])
                    line_road.append([road[j+1], (i+1)*10])
            if j == len(road)-2:
                line_road.append([road[j+1], 0])

        self.curveLine.setData(np.array(line_road))
        self.curveRoad.setData(np.array(pos_road))

        self.plot.setXRange(-pos[-1]-10, pos[-1]+10, padding=0)
        self.plot.setYRange(-20, len(pos)*10, padding=0)
        self.plot.showGrid(x = False, y = False, alpha = 0.3)

    def roadMove(self):
        if self.road:
            if self.timer_cnt >= len(self.road):
                self.timer_cnt = 0
            self.timer_cnt = (self.timer_cnt + 1) % len(self.road)
            self.curveRoadPoint.setPos(float(self.timer_cnt)/(len(self.road)-1))
            self.moveText.setHtml('<span style="color:red">%0.1f</span>' % float(self.road_step[self.timer_cnt]))
            x = self.road[self.timer_cnt][0]
            y = self.road[self.timer_cnt][1]
            z = self.road[self.timer_cnt][2]
            self.label.setText('''  <span style='color: red'>x=%0.1f,
                                    <span style='color: green'>y=%0.1f</span>,
                                    <span style='color: blue'>z=%0.1f</span>''' % (x,y,z))

