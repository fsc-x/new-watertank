#!/usr/bin/env python
# encoding: utf-8

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

from ui.scanMaxDose import Ui_Dialog as scanMaxDoseDialog
from win32api import GetSystemMetrics
from .ChartDeal import ChartDealWidget, tabView, GraphWidget

import re
import os
import math


class TabFile(QTabWidget):
    def __init__(self, parent=None):
        super(TabFile, self).__init__(parent)

        self.tab_chart = GraphWidget()
        self.tab_data = tabView()

        self.addTab(self.tab_chart, "表格")
        self.addTab(self.tab_data, "数据")

        self.tabChartUI()
        self.tabDataUI()

    def tabChartUI(self):
        layout = QHBoxLayout()
        self.setTabText(0, "表格")
        self.tab_chart.setLayout(layout)
        
    def tabDataUI(self):
        layout = QHBoxLayout()
        self.setTabText(1, "数据")
        self.tab_data.setLayout(layout)

class ScanMaxDoseDialog(QDialog, scanMaxDoseDialog):
    def __init__(self,parent=None):
        super(QDialog, self).__init__(parent)
        self.setupUi(self)

        #  窗口尺寸设计
        w = GetSystemMetrics (0)/1.2
        h = GetSystemMetrics (1)/1.2
        self.setFixedSize(w,h)
        self.setWindowTitle("最大吸收剂量比")
        self.doubleSpinBox.valueChanged.connect(self.getResult)

    def getResult(self):
        r = self.doubleSpinBox.value()
        res = []
        # for x, y, f in self.data:
        for x, y, f in zip(self.x_list, self.y_list, self.f_list):
            for idx in range(len(x)-1):
                if str(f).find("AGBT") > 0 | str(f).find("BTAG") > 0 | str(f).find("ATBG") | str(f).find("BGAT"):
                    # 对角线
                    if abs(x[idx]) <= (r / math.sin(math.pi/4)):
                        res.append(y[idx])
                else:
                    if abs(x[idx]) <= r:
                        res.append(y[idx])
        if not res:
            QMessageBox.information(self, "提示", "范围内无测量数据,请重新输入范围!", QMessageBox.Yes)
            self.label.setText("")
            return
        max_val = max(res)
        average = float(sum(res))/len(res)
        rate = max_val/average

        html = """ <div>
                    <span style="color: #ff0000;font-size: 12pt;float:left">均值:</span>
                    <span style="color: #000000;font-size: 12pt;float:right">%.3f</span>
                    <br>
                    <span style="color: #ff0000;font-size: 12pt;float:left">最大值:</span>
                    <span style="color: #000000;font-size: 12pt;float:right">%.3f</span>
                    <br>
                    <span style="color: #ff0000;font-size: 12pt;float:left">最大吸收剂量比:</span>
                    <span style="color: #000000;font-size: 12pt;float:right">%.3f</span>
                    <br>
                   </div>
            """ % (average, max_val, rate)
        self.label.setText(html)

    def setData(self, x_list, y_list, f_list):
        # self.data = zip(x_list, y_list, f_list) 
        self.x_list = x_list 
        self.y_list = y_list 
        self.f_list = f_list 
        self.getResult()

    def setFiles(self, file_list):
        data = []
        conf = []
        tab = QTabWidget()
        for i in range(len(file_list)):
            f = file_list[i] 
            chartDealDlg = ChartDealWidget()
            chartDealDlg.setWindowIcon(QIcon(':icons/images_rc/chart.png'))
            chartDealDlg.showOneLine(f)
            tab.addTab(chartDealDlg, str(f.split('/')[-1]))
        self.horizontalLayout.addWidget(tab)

if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    win = OarDialog()
    win.show()
    app.exec_()

