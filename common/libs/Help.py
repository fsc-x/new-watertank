#!/usr/bin/env python
# encoding: utf-8

import datetime

class Helper():

    '''
    获取格式化时间
    '''
    @staticmethod
    def getFormatTime(date = None, format = '%Y-%m-%d %H:%M:%S'):
        if date is None:
            date = datetime.datetime.now()
        return date.strftime(format)

    '''
    获取当前时间
    '''
    @staticmethod
    def getCurrentTime(format = '%Y-%m-%d %H:%M:%S'):
        return datetime.datetime.now()


