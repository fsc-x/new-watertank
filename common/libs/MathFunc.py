

class MathFunc:

    @staticmethod
    def intToHexStr(num=None, length=4):
        """int类型（包含正负数）转换为16进制字符串： -100  -->  0xFF9C"""
        if num is not None:
            tohex = lambda val, nbits: hex((val + (1 << nbits)) % (1 << nbits))
            return tohex(int(num), 16).replace('0x', '').zfill(length).upper()
        return False

    @staticmethod
    def hexStrToInt(hex_str=None):
        """hex字符串转换为int类型:
        negative:   True-> 转换为负数 , False->转换为正数

        负数转换：0xFF9C - > -100
        """
        if hex_str:
            if hex_str[0] == 'F' or hex_str[0] == 'f':
                return int(hex_str, 16) - (1 << 4*len(hex_str))
            else:
                return int(hex_str, 16)

    @staticmethod
    def hexStrToNeg(hex_str=None):
        if hex_str:
            if hex_str[0] == 'F' or hex_str[0] == 'f':
                return int(hex_str, 16) - (1 << 4*len(hex_str))
            else:
                return int(hex_str, 16)
        return False

        # if length > 0:
            # if negative:
                # if hex_str[0] == 'F' or hex_str[0] == 'f':
                    # return int(hex_str, 16) - (1 << 4*length)
                # else:
                    # return int(hex_str, 16)
            # else:
                # return int(hex_str, 16)
        # else:
            # return False



if __name__ == '__main__':
    math = MathFunc()
    res = math.intToHexStr(-100, 8)
    print(res)
    res = math.hexStrToInt(hex_str="ff9c")
    print(res)
