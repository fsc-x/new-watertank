#!/usr/bin/env python
# encoding: utf-8

import time
import logging
import os

# logging.basicConfig(level = logging.INFO,format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s')

filename = '%s/log/%s.log' % (os.getcwd(),
                              time.strftime("%Y%m%d", time.localtime()))
logging.basicConfig(level=logging.INFO,  # 控制台打印的日志级别
                    filename=filename,
                    filemode='a',  # 模式，有w和a，w就是写模式，每次都会重新写日志，覆盖之前的日志 
                    format='%(asctime)s - %(pathname)s[line:%(lineno)d] - %(levelname)s: %(message)s'
                    )

logger = logging.getLogger(__name__)
