# new-watertank

#### 介绍
三维水箱软件，实现水箱移动控制、数据处理

#### 软件架构

**系统框架图**

```mermaid
graph TD
A[主窗口] --> B(项目管理)
A[主窗口] --> C(通信串口)
A[主窗口] --> D(本地测量)
A[主窗口] --> E(重复性测量)
A[主窗口] --> F(3D图形)
A[主窗口] --> G(曲线表格)
A[主窗口] --> H(电机操作)
A[主窗口] --> I(PDD任务添加)
A[主窗口] --> J(OAR任务添加)
A[主窗口] --> K(任务执行)
```

**目录说明**
```
|—— new-watertank                             # 主目录
|   |—— build                                 # 构建
|   |—— common                                # 公共库
|   |   |—— ComDeal.py                        # 串口通信相关处理
|   |   |—— Help.py                           # 公共函数
|   |   |—— Log.py                            # 日志
|   |   |—— MathFunc.py                       # 数学相关函数
|   |—— conf                                  # 配置文件
|   |—— data                                  # 测量数据
|   |—— dist                                  # 打包发布
|   |—— doc                                   # 文档
|   |—— log                                   # 日志
|   |—— packages                              # 打包程序需要的包
|   |—— projects                              # 项目管理
|   |—— TMR                                   # TMR处理, 外部调用
|   |—— ui                                    # ui界面
|   |   |—— images_rc                         # 图片和图标
|   |   |—— compile.py                        # ui编译脚本, python compile.py xxx.ui
|   |—— widgets                               # 各个窗口
|   |   |—— app                               # 主窗口app
|   |   |—— base                              # 基本设置
|   |   |—— environment                       # 本底测量窗口
|   |   |—— motor                             # 电机控制窗口
|   |   |—— project                           # 项目管理窗口
|   |   |   |—— AddOarDialog.py               # 添加Oar任务窗口
|   |   |   |—— AddPddDialog.py               # 添加Pdd任务窗口
|   |   |   |—— ChartDeal.py                  # 曲线处理，pdd、oar曲线算法处理
|   |   |   |—— MultiLineAnalyseDialog.py     # 多曲线分析窗口
|   |   |   |—— ProjectManager.py             # 项目管理
|   |   |   |—— RayGraphWidget.py             # 3D射线水箱示意图窗口
|   |   |   |—— RoadChartWidget.py            # 走位测量任务路线示意图窗口 
|   |   |   |—— SameDoseDialog.py             # 等剂量曲线图窗口
|   |   |   |—— ScanMaxDoseDialog.py          # 最大吸收剂量窗口
|   |   |   |—— TaskRunManager.py             # 走位测量任务运行管理
|   |   |—— repeat                            # 重复性测量窗口
|   |   |—— serial                            # 串口设置窗口-串口通信处理
|   |—— app.ico                               # 软件图标
|   |—— application.py                        # 运行入口, python application.py
|   |—— demo.py                               # 测试使用
|   |—— install_RG202.iss                     # 打包exe脚本
|   |—— pyinstaller_RG202.spec                # pyinstaller打包脚本
```

#### 开发

```python
# python版本
python-3.7.0

# 克隆项目
git clone https://gitee.com/fsc-x/new-watertank.git

# 进入目录
cd new-watertank

# 安装依赖
pip install -r requirements.txt

# 运行程序
python application.py
```

#### 发布

```
# 打包程序
pyinstaller pyinstaller_RG202.spec
```

