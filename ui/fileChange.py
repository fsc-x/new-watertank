# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'fileChange.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(982, 570)
        self.gridLayout = QtWidgets.QGridLayout(Form)
        self.gridLayout.setObjectName("gridLayout")
        self.listWidget = QtWidgets.QListWidget(Form)
        self.listWidget.setObjectName("listWidget")
        self.gridLayout.addWidget(self.listWidget, 1, 0, 1, 2)
        self.pushButton_txt2asc = QtWidgets.QPushButton(Form)
        self.pushButton_txt2asc.setObjectName("pushButton_txt2asc")
        self.gridLayout.addWidget(self.pushButton_txt2asc, 0, 1, 1, 1)
        self.pushButton_selectFile = QtWidgets.QPushButton(Form)
        self.pushButton_selectFile.setObjectName("pushButton_selectFile")
        self.gridLayout.addWidget(self.pushButton_selectFile, 0, 0, 1, 1)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "三维水箱文件格式转换工具"))
        self.pushButton_txt2asc.setText(_translate("Form", "txt、excel导出asc"))
        self.pushButton_selectFile.setText(_translate("Form", "选择文件"))

