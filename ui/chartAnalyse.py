# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'chartAnalyse.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(1284, 764)
        self.gridLayout_7 = QtWidgets.QGridLayout(Form)
        self.gridLayout_7.setObjectName("gridLayout_7")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_7.addItem(spacerItem, 0, 0, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_7.addItem(spacerItem1, 0, 2, 1, 1)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_7.addItem(spacerItem2, 0, 3, 1, 1)
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.pushButton_multiLine = QtWidgets.QPushButton(Form)
        self.pushButton_multiLine.setObjectName("pushButton_multiLine")
        self.gridLayout.addWidget(self.pushButton_multiLine, 0, 1, 1, 1)
        self.pushButton_exportExcel = QtWidgets.QPushButton(Form)
        self.pushButton_exportExcel.setObjectName("pushButton_exportExcel")
        self.gridLayout.addWidget(self.pushButton_exportExcel, 0, 0, 1, 1)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.gridLayout.addLayout(self.verticalLayout, 1, 0, 1, 4)
        self.pushButton_exportTmr = QtWidgets.QPushButton(Form)
        self.pushButton_exportTmr.setObjectName("pushButton_exportTmr")
        self.gridLayout.addWidget(self.pushButton_exportTmr, 0, 2, 1, 1)
        self.pushButton_analyseTmr = QtWidgets.QPushButton(Form)
        self.pushButton_analyseTmr.setObjectName("pushButton_analyseTmr")
        self.gridLayout.addWidget(self.pushButton_analyseTmr, 0, 3, 1, 1)
        self.gridLayout_7.addLayout(self.gridLayout, 1, 0, 1, 1)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.tabWidget = QtWidgets.QTabWidget(Form)
        self.tabWidget.setObjectName("tabWidget")
        self.tab_pdd = QtWidgets.QWidget()
        self.tab_pdd.setObjectName("tab_pdd")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.tab_pdd)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.verticalLayout_pdd = QtWidgets.QVBoxLayout()
        self.verticalLayout_pdd.setContentsMargins(13, 13, 13, 13)
        self.verticalLayout_pdd.setObjectName("verticalLayout_pdd")
        self.gridLayout_2.addLayout(self.verticalLayout_pdd, 0, 0, 1, 1)
        self.tabWidget.addTab(self.tab_pdd, "")
        self.tab_X = QtWidgets.QWidget()
        self.tab_X.setObjectName("tab_X")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.tab_X)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.verticalLayout_x = QtWidgets.QVBoxLayout()
        self.verticalLayout_x.setContentsMargins(13, 13, 13, 13)
        self.verticalLayout_x.setObjectName("verticalLayout_x")
        self.gridLayout_3.addLayout(self.verticalLayout_x, 0, 0, 1, 1)
        self.tabWidget.addTab(self.tab_X, "")
        self.tab_Y = QtWidgets.QWidget()
        self.tab_Y.setObjectName("tab_Y")
        self.gridLayout_4 = QtWidgets.QGridLayout(self.tab_Y)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.verticalLayout_y = QtWidgets.QVBoxLayout()
        self.verticalLayout_y.setContentsMargins(13, 13, 13, 13)
        self.verticalLayout_y.setObjectName("verticalLayout_y")
        self.gridLayout_4.addLayout(self.verticalLayout_y, 0, 0, 1, 1)
        self.tabWidget.addTab(self.tab_Y, "")
        self.tab_3D = QtWidgets.QWidget()
        self.tab_3D.setObjectName("tab_3D")
        self.gridLayout_5 = QtWidgets.QGridLayout(self.tab_3D)
        self.gridLayout_5.setObjectName("gridLayout_5")
        self.verticalLayout_3d = QtWidgets.QVBoxLayout()
        self.verticalLayout_3d.setContentsMargins(13, 13, 13, 13)
        self.verticalLayout_3d.setObjectName("verticalLayout_3d")
        self.gridLayout_5.addLayout(self.verticalLayout_3d, 0, 0, 1, 1)
        self.tabWidget.addTab(self.tab_3D, "")
        self.tab_table = QtWidgets.QWidget()
        self.tab_table.setObjectName("tab_table")
        self.gridLayout_6 = QtWidgets.QGridLayout(self.tab_table)
        self.gridLayout_6.setObjectName("gridLayout_6")
        self.verticalLayout_table = QtWidgets.QVBoxLayout()
        self.verticalLayout_table.setContentsMargins(13, 13, 13, 13)
        self.verticalLayout_table.setObjectName("verticalLayout_table")
        self.gridLayout_6.addLayout(self.verticalLayout_table, 0, 0, 1, 1)
        self.tabWidget.addTab(self.tab_table, "")
        self.verticalLayout_2.addWidget(self.tabWidget)
        self.gridLayout_7.addLayout(self.verticalLayout_2, 1, 1, 1, 3)

        self.retranslateUi(Form)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "测量曲线结果分析"))
        self.pushButton_multiLine.setText(_translate("Form", "多曲线分析"))
        self.pushButton_exportExcel.setText(_translate("Form", "导出excel"))
        self.pushButton_exportTmr.setText(_translate("Form", "导出tmr"))
        self.pushButton_analyseTmr.setText(_translate("Form", "tmr分析"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_pdd), _translate("Form", "OG方向"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_X), _translate("Form", "AB方向"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_Y), _translate("Form", "GT方向"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_3D), _translate("Form", "3D显示"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_table), _translate("Form", "表格数据"))

import images_rc
