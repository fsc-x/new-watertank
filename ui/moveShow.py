# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'moveShow.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_DockWidget(object):
    def setupUi(self, DockWidget):
        DockWidget.setObjectName("DockWidget")
        DockWidget.resize(585, 569)
        self.dockWidgetContents = QtWidgets.QWidget()
        self.dockWidgetContents.setObjectName("dockWidgetContents")
        self.gridLayout = QtWidgets.QGridLayout(self.dockWidgetContents)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.gridLayout.addLayout(self.verticalLayout, 1, 0, 1, 3)
        self.label_z = QtWidgets.QLabel(self.dockWidgetContents)
        self.label_z.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.label_z.setFont(font)
        self.label_z.setStyleSheet("color: rgb(255, 0, 0);")
        self.label_z.setTextFormat(QtCore.Qt.AutoText)
        self.label_z.setObjectName("label_z")
        self.gridLayout.addWidget(self.label_z, 0, 2, 1, 1)
        self.label_y = QtWidgets.QLabel(self.dockWidgetContents)
        self.label_y.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.label_y.setFont(font)
        self.label_y.setStyleSheet("color: rgb(255, 0, 0);")
        self.label_y.setTextFormat(QtCore.Qt.AutoText)
        self.label_y.setObjectName("label_y")
        self.gridLayout.addWidget(self.label_y, 0, 1, 1, 1)
        self.label_x = QtWidgets.QLabel(self.dockWidgetContents)
        self.label_x.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.label_x.setFont(font)
        self.label_x.setStyleSheet("color: rgb(255, 0, 0);")
        self.label_x.setTextFormat(QtCore.Qt.AutoText)
        self.label_x.setObjectName("label_x")
        self.gridLayout.addWidget(self.label_x, 0, 0, 1, 1)
        DockWidget.setWidget(self.dockWidgetContents)

        self.retranslateUi(DockWidget)
        QtCore.QMetaObject.connectSlotsByName(DockWidget)

    def retranslateUi(self, DockWidget):
        _translate = QtCore.QCoreApplication.translate
        DockWidget.setWindowTitle(_translate("DockWidget", "移动示意"))
        self.label_z.setText(_translate("DockWidget", "z"))
        self.label_y.setText(_translate("DockWidget", "y"))
        self.label_x.setText(_translate("DockWidget", "x"))

