# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'createProject.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(693, 415)
        self.gridLayout = QtWidgets.QGridLayout(Dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.lineEdit_person = QtWidgets.QLineEdit(Dialog)
        self.lineEdit_person.setObjectName("lineEdit_person")
        self.gridLayout.addWidget(self.lineEdit_person, 2, 1, 1, 1)
        self.label_5 = QtWidgets.QLabel(Dialog)
        self.label_5.setObjectName("label_5")
        self.gridLayout.addWidget(self.label_5, 2, 0, 1, 1)
        self.pushButton_getTime = QtWidgets.QPushButton(Dialog)
        self.pushButton_getTime.setObjectName("pushButton_getTime")
        self.gridLayout.addWidget(self.pushButton_getTime, 4, 2, 1, 1)
        self.lineEdit_addr = QtWidgets.QLineEdit(Dialog)
        self.lineEdit_addr.setObjectName("lineEdit_addr")
        self.gridLayout.addWidget(self.lineEdit_addr, 3, 1, 1, 1)
        self.lineEdit_time = QtWidgets.QLineEdit(Dialog)
        self.lineEdit_time.setObjectName("lineEdit_time")
        self.gridLayout.addWidget(self.lineEdit_time, 4, 1, 1, 1)
        self.label_4 = QtWidgets.QLabel(Dialog)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 4, 0, 1, 1)
        self.label_3 = QtWidgets.QLabel(Dialog)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 3, 0, 1, 1)
        self.pushButton_open = QtWidgets.QPushButton(Dialog)
        self.pushButton_open.setObjectName("pushButton_open")
        self.gridLayout.addWidget(self.pushButton_open, 0, 2, 1, 1)
        self.lineEdit_name = QtWidgets.QLineEdit(Dialog)
        self.lineEdit_name.setObjectName("lineEdit_name")
        self.gridLayout.addWidget(self.lineEdit_name, 1, 1, 1, 1)
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout.addWidget(self.buttonBox, 7, 0, 1, 3)
        self.lineEdit_path = QtWidgets.QLineEdit(Dialog)
        self.lineEdit_path.setObjectName("lineEdit_path")
        self.gridLayout.addWidget(self.lineEdit_path, 0, 1, 1, 1)
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.textEdit_remark = QtWidgets.QTextEdit(Dialog)
        self.textEdit_remark.setObjectName("textEdit_remark")
        self.gridLayout.addWidget(self.textEdit_remark, 5, 1, 1, 1)
        self.label_6 = QtWidgets.QLabel(Dialog)
        self.label_6.setObjectName("label_6")
        self.gridLayout.addWidget(self.label_6, 5, 0, 1, 1)

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "新建项目"))
        self.label_5.setText(_translate("Dialog", "检测人员："))
        self.pushButton_getTime.setText(_translate("Dialog", "获取当前时间"))
        self.label_4.setText(_translate("Dialog", "检测时间："))
        self.label_3.setText(_translate("Dialog", "检测地点:"))
        self.pushButton_open.setText(_translate("Dialog", "..."))
        self.label_2.setText(_translate("Dialog", "项目名称:"))
        self.label.setText(_translate("Dialog", "项目路径："))
        self.label_6.setText(_translate("Dialog", "备注："))

