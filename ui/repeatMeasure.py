# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'repeatMeasure.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(820, 603)
        self.gridLayout = QtWidgets.QGridLayout(Dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.pushButton_stopMeasure = QtWidgets.QPushButton(Dialog)
        self.pushButton_stopMeasure.setObjectName("pushButton_stopMeasure")
        self.gridLayout.addWidget(self.pushButton_stopMeasure, 0, 5, 1, 1)
        self.pushButton_clearTable = QtWidgets.QPushButton(Dialog)
        self.pushButton_clearTable.setObjectName("pushButton_clearTable")
        self.gridLayout.addWidget(self.pushButton_clearTable, 0, 6, 1, 1)
        self.comboBox = QtWidgets.QComboBox(Dialog)
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.gridLayout.addWidget(self.comboBox, 0, 1, 1, 1)
        self.pushButton_setMeasureCycle = QtWidgets.QPushButton(Dialog)
        self.pushButton_setMeasureCycle.setObjectName("pushButton_setMeasureCycle")
        self.gridLayout.addWidget(self.pushButton_setMeasureCycle, 0, 3, 1, 1)
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 0, 2, 1, 1)
        self.pushButton_startMeasure = QtWidgets.QPushButton(Dialog)
        self.pushButton_startMeasure.setObjectName("pushButton_startMeasure")
        self.gridLayout.addWidget(self.pushButton_startMeasure, 0, 4, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(9, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 0, 8, 1, 1)
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.tableView = QtWidgets.QTableView(Dialog)
        self.tableView.setObjectName("tableView")
        self.verticalLayout.addWidget(self.tableView)
        self.gridLayout.addLayout(self.verticalLayout, 1, 0, 1, 9)
        self.pushButton_getResult = QtWidgets.QPushButton(Dialog)
        self.pushButton_getResult.setObjectName("pushButton_getResult")
        self.gridLayout.addWidget(self.pushButton_getResult, 0, 7, 1, 1)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "重复性测量"))
        self.pushButton_stopMeasure.setText(_translate("Dialog", "停止测量"))
        self.pushButton_clearTable.setText(_translate("Dialog", "清空表格"))
        self.comboBox.setItemText(0, _translate("Dialog", "200"))
        self.comboBox.setItemText(1, _translate("Dialog", "500"))
        self.comboBox.setItemText(2, _translate("Dialog", "1000"))
        self.comboBox.setItemText(3, _translate("Dialog", "2000"))
        self.pushButton_setMeasureCycle.setText(_translate("Dialog", "设置测量周期"))
        self.label_2.setText(_translate("Dialog", "ms"))
        self.pushButton_startMeasure.setText(_translate("Dialog", "开始测量"))
        self.label.setText(_translate("Dialog", "周期："))
        self.pushButton_getResult.setText(_translate("Dialog", "计算"))

