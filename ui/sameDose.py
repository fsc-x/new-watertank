# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'sameDose.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(460, 323)
        self.gridLayout = QtWidgets.QGridLayout(Dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.tabWidget = QtWidgets.QTabWidget(Dialog)
        self.tabWidget.setObjectName("tabWidget")
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.tab)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.label_2 = QtWidgets.QLabel(self.tab)
        self.label_2.setObjectName("label_2")
        self.gridLayout_2.addWidget(self.label_2, 0, 4, 1, 1)
        self.doubleSpinBox_unit = QtWidgets.QDoubleSpinBox(self.tab)
        self.doubleSpinBox_unit.setMinimum(1.0)
        self.doubleSpinBox_unit.setMaximum(10.0)
        self.doubleSpinBox_unit.setProperty("value", 2.0)
        self.doubleSpinBox_unit.setObjectName("doubleSpinBox_unit")
        self.gridLayout_2.addWidget(self.doubleSpinBox_unit, 0, 3, 1, 1)
        self.spinBox = QtWidgets.QSpinBox(self.tab)
        self.spinBox.setEnabled(False)
        self.spinBox.setProperty("value", 1)
        self.spinBox.setObjectName("spinBox")
        self.gridLayout_2.addWidget(self.spinBox, 0, 2, 1, 1)
        self.label = QtWidgets.QLabel(self.tab)
        self.label.setObjectName("label")
        self.gridLayout_2.addWidget(self.label, 0, 1, 1, 1)
        self.doubleSpinBox = QtWidgets.QDoubleSpinBox(self.tab)
        self.doubleSpinBox.setMaximum(100.0)
        self.doubleSpinBox.setProperty("value", 20.0)
        self.doubleSpinBox.setObjectName("doubleSpinBox")
        self.gridLayout_2.addWidget(self.doubleSpinBox, 0, 5, 1, 1)
        self.pushButton_getResult = QtWidgets.QPushButton(self.tab)
        self.pushButton_getResult.setObjectName("pushButton_getResult")
        self.gridLayout_2.addWidget(self.pushButton_getResult, 0, 0, 1, 1)
        self.pushButton_getRate = QtWidgets.QPushButton(self.tab)
        self.pushButton_getRate.setObjectName("pushButton_getRate")
        self.gridLayout_2.addWidget(self.pushButton_getRate, 0, 6, 1, 1)
        self.verticalLayout_same = QtWidgets.QVBoxLayout()
        self.verticalLayout_same.setObjectName("verticalLayout_same")
        self.gridLayout_2.addLayout(self.verticalLayout_same, 1, 0, 1, 7)
        self.tabWidget.addTab(self.tab, "")
        self.tab_3 = QtWidgets.QWidget()
        self.tab_3.setObjectName("tab_3")
        self.gridLayout_4 = QtWidgets.QGridLayout(self.tab_3)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.verticalLayout_3d = QtWidgets.QVBoxLayout()
        self.verticalLayout_3d.setObjectName("verticalLayout_3d")
        self.gridLayout_4.addLayout(self.verticalLayout_3d, 0, 0, 1, 1)
        self.tabWidget.addTab(self.tab_3, "")
        self.tab_2 = QtWidgets.QWidget()
        self.tab_2.setObjectName("tab_2")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.tab_2)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.gridLayout_3.addLayout(self.horizontalLayout, 0, 0, 1, 1)
        self.tabWidget.addTab(self.tab_2, "")
        self.gridLayout.addWidget(self.tabWidget, 0, 0, 1, 1)

        self.retranslateUi(Dialog)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.label_2.setText(_translate("Dialog", "百分比(%)"))
        self.label.setText(_translate("Dialog", "分割比："))
        self.pushButton_getResult.setText(_translate("Dialog", "重绘曲线"))
        self.pushButton_getRate.setText(_translate("Dialog", "计算比例"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("Dialog", "等剂量分布"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_3), _translate("Dialog", "3D"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), _translate("Dialog", "原始数据"))

