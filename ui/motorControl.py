# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'motorControl.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(757, 598)
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.groupBox = QtWidgets.QGroupBox(Dialog)
        self.groupBox.setObjectName("groupBox")
        self.gridLayout = QtWidgets.QGridLayout(self.groupBox)
        self.gridLayout.setObjectName("gridLayout")
        self.pushButton_stop = QtWidgets.QPushButton(self.groupBox)
        self.pushButton_stop.setObjectName("pushButton_stop")
        self.gridLayout.addWidget(self.pushButton_stop, 1, 3, 1, 1)
        self.doubleSpinBox_y = QtWidgets.QDoubleSpinBox(self.groupBox)
        self.doubleSpinBox_y.setObjectName("doubleSpinBox_y")
        self.gridLayout.addWidget(self.doubleSpinBox_y, 1, 1, 1, 1)
        self.doubleSpinBox_z = QtWidgets.QDoubleSpinBox(self.groupBox)
        self.doubleSpinBox_z.setObjectName("doubleSpinBox_z")
        self.gridLayout.addWidget(self.doubleSpinBox_z, 1, 2, 1, 1)
        self.pushButton_movePos = QtWidgets.QPushButton(self.groupBox)
        self.pushButton_movePos.setObjectName("pushButton_movePos")
        self.gridLayout.addWidget(self.pushButton_movePos, 0, 3, 1, 1)
        self.label_y = QtWidgets.QLabel(self.groupBox)
        self.label_y.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.label_y.setFont(font)
        self.label_y.setStyleSheet("color: rgb(255, 0, 0);")
        self.label_y.setTextFormat(QtCore.Qt.AutoText)
        self.label_y.setObjectName("label_y")
        self.gridLayout.addWidget(self.label_y, 0, 1, 1, 1)
        self.doubleSpinBox_x = QtWidgets.QDoubleSpinBox(self.groupBox)
        self.doubleSpinBox_x.setObjectName("doubleSpinBox_x")
        self.gridLayout.addWidget(self.doubleSpinBox_x, 1, 0, 1, 1)
        self.label_z = QtWidgets.QLabel(self.groupBox)
        self.label_z.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.label_z.setFont(font)
        self.label_z.setStyleSheet("color: rgb(255, 0, 0);")
        self.label_z.setTextFormat(QtCore.Qt.AutoText)
        self.label_z.setObjectName("label_z")
        self.gridLayout.addWidget(self.label_z, 0, 2, 1, 1)
        self.label_x = QtWidgets.QLabel(self.groupBox)
        self.label_x.setMaximumSize(QtCore.QSize(16777215, 40))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.label_x.setFont(font)
        self.label_x.setStyleSheet("color: rgb(255, 0, 0);")
        self.label_x.setTextFormat(QtCore.Qt.AutoText)
        self.label_x.setObjectName("label_x")
        self.gridLayout.addWidget(self.label_x, 0, 0, 1, 1)
        self.pushButton_backZero = QtWidgets.QPushButton(self.groupBox)
        self.pushButton_backZero.setObjectName("pushButton_backZero")
        self.gridLayout.addWidget(self.pushButton_backZero, 3, 0, 1, 1)
        self.pushButton_setZero = QtWidgets.QPushButton(self.groupBox)
        self.pushButton_setZero.setObjectName("pushButton_setZero")
        self.gridLayout.addWidget(self.pushButton_setZero, 3, 1, 1, 1)
        self.doubleSpinBox_x.raise_()
        self.doubleSpinBox_z.raise_()
        self.doubleSpinBox_y.raise_()
        self.label_x.raise_()
        self.label_y.raise_()
        self.label_z.raise_()
        self.pushButton_stop.raise_()
        self.pushButton_movePos.raise_()
        self.pushButton_backZero.raise_()
        self.pushButton_setZero.raise_()
        self.verticalLayout.addWidget(self.groupBox)
        self.groupBox_2 = QtWidgets.QGroupBox(Dialog)
        self.groupBox_2.setObjectName("groupBox_2")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.groupBox_2)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.lineEdit_limit_z2 = QtWidgets.QLineEdit(self.groupBox_2)
        self.lineEdit_limit_z2.setObjectName("lineEdit_limit_z2")
        self.gridLayout_2.addWidget(self.lineEdit_limit_z2, 4, 1, 1, 1)
        self.label_5 = QtWidgets.QLabel(self.groupBox_2)
        self.label_5.setObjectName("label_5")
        self.gridLayout_2.addWidget(self.label_5, 4, 2, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.groupBox_2)
        self.label_3.setObjectName("label_3")
        self.gridLayout_2.addWidget(self.label_3, 2, 2, 1, 1)
        self.pushButton_readLimit = QtWidgets.QPushButton(self.groupBox_2)
        self.pushButton_readLimit.setObjectName("pushButton_readLimit")
        self.gridLayout_2.addWidget(self.pushButton_readLimit, 0, 1, 1, 1)
        self.lineEdit_limit_y2 = QtWidgets.QLineEdit(self.groupBox_2)
        self.lineEdit_limit_y2.setObjectName("lineEdit_limit_y2")
        self.gridLayout_2.addWidget(self.lineEdit_limit_y2, 2, 1, 1, 1)
        self.lineEdit_limit_y1 = QtWidgets.QLineEdit(self.groupBox_2)
        self.lineEdit_limit_y1.setObjectName("lineEdit_limit_y1")
        self.gridLayout_2.addWidget(self.lineEdit_limit_y1, 2, 3, 1, 1)
        self.lineEdit_limit_z1 = QtWidgets.QLineEdit(self.groupBox_2)
        self.lineEdit_limit_z1.setObjectName("lineEdit_limit_z1")
        self.gridLayout_2.addWidget(self.lineEdit_limit_z1, 4, 3, 1, 1)
        self.label_6 = QtWidgets.QLabel(self.groupBox_2)
        self.label_6.setObjectName("label_6")
        self.gridLayout_2.addWidget(self.label_6, 4, 0, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.groupBox_2)
        self.label_4.setObjectName("label_4")
        self.gridLayout_2.addWidget(self.label_4, 2, 0, 1, 1)
        self.pushButton_setLimit = QtWidgets.QPushButton(self.groupBox_2)
        self.pushButton_setLimit.setObjectName("pushButton_setLimit")
        self.gridLayout_2.addWidget(self.pushButton_setLimit, 0, 3, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.groupBox_2)
        self.label_2.setObjectName("label_2")
        self.gridLayout_2.addWidget(self.label_2, 1, 0, 1, 1)
        self.lineEdit_limit_x2 = QtWidgets.QLineEdit(self.groupBox_2)
        self.lineEdit_limit_x2.setObjectName("lineEdit_limit_x2")
        self.gridLayout_2.addWidget(self.lineEdit_limit_x2, 1, 1, 1, 1)
        self.lineEdit_limit_x1 = QtWidgets.QLineEdit(self.groupBox_2)
        self.lineEdit_limit_x1.setObjectName("lineEdit_limit_x1")
        self.gridLayout_2.addWidget(self.lineEdit_limit_x1, 1, 3, 1, 1)
        self.label = QtWidgets.QLabel(self.groupBox_2)
        self.label.setObjectName("label")
        self.gridLayout_2.addWidget(self.label, 1, 2, 1, 1)
        self.verticalLayout.addWidget(self.groupBox_2)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "电机操作"))
        self.groupBox.setTitle(_translate("Dialog", "移动控制"))
        self.pushButton_stop.setText(_translate("Dialog", "停止"))
        self.pushButton_movePos.setText(_translate("Dialog", "移动"))
        self.label_y.setText(_translate("Dialog", "y"))
        self.label_z.setText(_translate("Dialog", "z"))
        self.label_x.setText(_translate("Dialog", "x"))
        self.pushButton_backZero.setText(_translate("Dialog", "回原点"))
        self.pushButton_setZero.setText(_translate("Dialog", "复位坐标"))
        self.groupBox_2.setTitle(_translate("Dialog", "限位设置"))
        self.lineEdit_limit_z2.setText(_translate("Dialog", "0"))
        self.label_5.setText(_translate("Dialog", "-"))
        self.label_3.setText(_translate("Dialog", "-"))
        self.pushButton_readLimit.setText(_translate("Dialog", "读取限位"))
        self.lineEdit_limit_y2.setText(_translate("Dialog", "0"))
        self.lineEdit_limit_y1.setText(_translate("Dialog", "0"))
        self.lineEdit_limit_z1.setText(_translate("Dialog", "0"))
        self.label_6.setText(_translate("Dialog", "Z"))
        self.label_4.setText(_translate("Dialog", "Y"))
        self.pushButton_setLimit.setText(_translate("Dialog", "设置限位"))
        self.label_2.setText(_translate("Dialog", "X"))
        self.lineEdit_limit_x2.setText(_translate("Dialog", "0"))
        self.lineEdit_limit_x1.setText(_translate("Dialog", "0"))
        self.label.setText(_translate("Dialog", "-"))

