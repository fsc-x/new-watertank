from PyQt5.QtWidgets import QTextBrowser
from PyQt5.QtGui import QTextCursor
import random
class textBrowser_color(QTextBrowser):
    color_list = [
        'FF0000',#Red
        'FF1493',#DeepPink
        '00FF00',#Green
        '7FFF00',#Chartreuse
        'FFFF00',#Yellow
        '0000FF',#Blue
        '9B30FF',#Purple1
        'FFA500',#Orange
        'FF8C00',#DarkOrange
        '00FFFF',#Cyan
    ]
    def Clear(self):
        self.setText('')

    def stringToHtmlFilter(self, str):
        str.replace("&", "&amp;")
        str.replace(">", "&gt;")
        str.replace("<", "&lt;")
        str.replace("\"", "&quot;")
        str.replace("\'", "&#39;")
        str.replace(" ", "&nbsp;")
        str.replace("\n", "<br>")
        str.replace("\r", "<br>")
        return str

    def stringToHtml(self, str, crl):
        return "<span style=\" color:#%s;\">%s</span>"%(crl, str)

    def setText_HTML(self,p_str, c_str):
        p_str = self.stringToHtmlFilter(p_str)
        p_str = self.stringToHtml(c_str,p_str)
        self.setText(p_str)

    def append_HTML(self, p_str, **kwargs):
        try:
            if kwargs['color'] == 'red':
                c_str = 'FF0000'
            elif kwargs['color'] == 'green':
                c_str = '00FF00'
            elif kwargs['color'] == 'blue':
                c_str = '0000FF'
            elif kwargs['color'] == 'white':
                c_str = 'FFFFFF'
            elif kwargs['color'] == 'black':
                c_str = 'FFFFFF'
            else:
                c_str = 'FFFFFF'
        except:
            c_num = random.randint(0, len(self.color_list)-1)
            c_str = self.color_list[c_num]
        p_str = "<span style=\" color:#%s;\">%s</span>"%(c_str, p_str)
        self.append(p_str)
        self.moveCursor(QTextCursor.End)


