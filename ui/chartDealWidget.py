# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'chartDealWidget.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1045, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setObjectName("tabWidget")
        self.tab_5 = QtWidgets.QWidget()
        self.tab_5.setObjectName("tab_5")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.tab_5)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.verticalLayout_chart = QtWidgets.QVBoxLayout()
        self.verticalLayout_chart.setObjectName("verticalLayout_chart")
        self.gridLayout_3.addLayout(self.verticalLayout_chart, 0, 0, 1, 1)
        self.tabWidget.addTab(self.tab_5, "")
        self.tab_6 = QtWidgets.QWidget()
        self.tab_6.setObjectName("tab_6")
        self.gridLayout_4 = QtWidgets.QGridLayout(self.tab_6)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.verticalLayout_table = QtWidgets.QVBoxLayout()
        self.verticalLayout_table.setObjectName("verticalLayout_table")
        self.gridLayout_4.addLayout(self.verticalLayout_table, 0, 0, 1, 1)
        self.tabWidget.addTab(self.tab_6, "")
        self.gridLayout.addWidget(self.tabWidget, 0, 0, 2, 1)
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setObjectName("groupBox")
        self.gridLayout_5 = QtWidgets.QGridLayout(self.groupBox)
        self.gridLayout_5.setObjectName("gridLayout_5")
        self.label_condition = QtWidgets.QLabel(self.groupBox)
        self.label_condition.setObjectName("label_condition")
        self.gridLayout_5.addWidget(self.label_condition, 0, 0, 1, 1)
        self.gridLayout.addWidget(self.groupBox, 0, 1, 1, 1)
        self.groupBox_result = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_result.setObjectName("groupBox_result")
        self.gridLayout_6 = QtWidgets.QGridLayout(self.groupBox_result)
        self.gridLayout_6.setObjectName("gridLayout_6")
        self.label_result = QtWidgets.QLabel(self.groupBox_result)
        self.label_result.setObjectName("label_result")
        self.gridLayout_6.addWidget(self.label_result, 0, 0, 1, 1)
        self.gridLayout.addWidget(self.groupBox_result, 1, 1, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1045, 30))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_5), _translate("MainWindow", "曲线"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_6), _translate("MainWindow", "表格"))
        self.groupBox.setTitle(_translate("MainWindow", "条件"))
        self.label_condition.setText(_translate("MainWindow", "TextLabel"))
        self.groupBox_result.setTitle(_translate("MainWindow", "计算结果"))
        self.label_result.setText(_translate("MainWindow", "TextLabel"))

