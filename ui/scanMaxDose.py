# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'scanMaxDose.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(402, 293)
        self.gridLayout = QtWidgets.QGridLayout(Dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.groupBox_result = QtWidgets.QGroupBox(Dialog)
        self.groupBox_result.setObjectName("groupBox_result")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.groupBox_result)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.doubleSpinBox = QtWidgets.QDoubleSpinBox(self.groupBox_result)
        self.doubleSpinBox.setProperty("value", 10.0)
        self.doubleSpinBox.setObjectName("doubleSpinBox")
        self.gridLayout_2.addWidget(self.doubleSpinBox, 0, 1, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.groupBox_result)
        self.label_2.setObjectName("label_2")
        self.gridLayout_2.addWidget(self.label_2, 0, 0, 1, 1)
        self.label = QtWidgets.QLabel(self.groupBox_result)
        self.label.setObjectName("label")
        self.gridLayout_2.addWidget(self.label, 1, 0, 1, 2)
        self.gridLayout.addWidget(self.groupBox_result, 0, 1, 2, 1)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.gridLayout.addLayout(self.horizontalLayout, 0, 0, 2, 1)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.groupBox_result.setTitle(_translate("Dialog", "计算结果"))
        self.label_2.setText(_translate("Dialog", "范围(mm)"))
        self.label.setText(_translate("Dialog", "TextLabel"))

